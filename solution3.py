# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 20:53:24 2015
@author: Charles
"""
import numpy as np
import pandas.io.data as download
import sys,getopt


#function to calculate the weight for the minmum variance 
def minmum_variance(stock):         #input is the stock price
    nrow=np.shape(stock)[0]-1       
    daily_return = stock.shift(-1)/stock - 1
    daily_return =daily_return[0:nrow]       #calculate the daily return 
    Iden=np.matrix([1]*3)
    cov_inv=np.linalg.inv(daily_return.cov())
    weights=cov_inv*Iden.transpose()/((Iden*cov_inv*Iden.transpose()))
    return weights
    
    

def main(argv):
   inputfile = ''
   outputfile= ''
   try:		
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])		
   except getopt.GetoptError:		
      print 'solution3.py -i <inputfile> -o <outputfile>'		
      sys.exit(2)		
   for opt, arg in opts:		
      if opt == '-h':		
         print 'solution3.py -i <inputfile> -o <outputfile>'		
         sys.exit()		
      elif opt in ("-i", "--ifile"):		
         inputfile = arg		
      elif opt in ("-o", "--ofile"):		
         outputfile = arg                         #condition for the input 
         
   input=open(inputfile,'rd').read().split('\r\n')
   data = download.DataReader(input, 'yahoo', '2014-10-31', '2015-10-31')
      # Quandl.get('GOOG',trim_start='2014-10-31', trim_end='2015-10-31', collapse='daily')
   stock = data['Adj Close']
   weights=minmum_variance(stock)
   output=open(outputfile,'w')
   tickers=open(inputfile,'rd').read().split('\r\n')   

   for i in range(3):              #output the results
       output.write(tickers[i]+','+str(weights[i,0]*100)+'%'+'\n')
   output.close()
   
       
   
  
       

    
       
    

if __name__ == "__main__":
   main(sys.argv[1:])
