#! /usr/bin/python
import datetime
import sys
from math import exp,log 
from itertools import count
from bisect import bisect

def mvmonth(cur_date, m):
  y, m = m/12, m % 12
  cury, curm, curd = cur_date.year + y, cur_date.month + m, cur_date.day
  return datetime.date(cury + 1, curm % 12, curd) if curm > 12 else datetime.date(cury, curm, curd)

def dfunc(func, x, eps = 1e-5):
  def dfeval(d):
    return (func(x+pow(2,-d))-func(x-pow(2,-d))) / pow(2,-d+1)

  def refiner(m, dfs):
    a,b = dfs.next(),dfs.next()
    while True:
      yield  (pow(4,m)*b - a) / (pow(4,m) - 1)
      a,b = b,dfs.next()

  res = 0
  #for vd in refiner(3,refiner(2,refiner(1,((dfeval(d) for d in count(9)))))):
  for vd in (dfeval(d) for d in count(9)):
    if abs(vd-res) < eps:
      break
    res = vd
  return res  

def newton_solve(func, dfunc, x0, eps = 1e-5):
  def roots(x):
    while True:
      k = dfunc(func, x, eps)
      x = x - 1*func(x) / k if k!=0 else x 
      yield x
  for root in roots(x0):
    if func(root) < 1:
      return root

def calc_price(coup, par, matday, today, paydays, ycdays, rates):
  def calc_pay(pd):
    p = bisect(ycdays, pd) - 1
    off, interval = pd - ycdays[p], ycdays[p+1] - ycdays[p]
    rate = (rates[p] + (rates[p+1] - rates[p]) * off / interval)
    return (coup + (0 if pd < matday else par)) * exp(-rate * pd)
  return sum([calc_pay(pd) for pd in paydays])

def calc_parate(coup, par, matday, today, paydays, r):
  def pricefunc(x):
    pp = calc_price(coup, par, matday, today, paydays, [0, 2e6], [x,x]) 
    print "price@",x,"=",pp
    return pp - par
  return newton_solve(pricefunc, dfunc, r, 1e-5)

def calc_duration(coup, par, matday, today, paydays, r):
  def pricefunc_wrap(x):
    return log(calc_price(coup, par, matday, today, paydays, [0, 2e6], [x,x])) 
  return -dfunc(pricefunc_wrap, r, 1e-5)

def calc_convexity(coup, par, matday, today, paydays, r, d):
  def duration_wrap(x):
    return calc_duration(coup, par, matday, today, paydays, x)
  return d*d - dfunc(duration_wrap, r, 1e-5)

def bondinfo(bondtype, par, mat, crate, comp, today, yc):
  coup, matday = par * crate * 0.01 / comp, (mat - today).days / 365.0
  ycdays = [float(ln.strip().split(',')[0]) for ln in yc]
  rates = [float(ln.strip().split(',')[1])*0.01 for ln in yc]
  paydays = [(pd - today).days / 365.0 for pd in ((mvmonth(mat,-m) for m in xrange(0,999,6))) if pd > today]
  # calc price
  price = calc_price(coup, par, matday, today, paydays, ycdays, rates)
  # calc par_rate
  par_rate = calc_parate(coup, par, matday, today, paydays, rates[-1])
  # calc duration
  duration = calc_duration(coup, par, matday, today, paydays, par_rate) 
  # calc convexity
  convexity = calc_convexity(coup, par, matday, today, paydays, par_rate, duration)
  return price, par_rate, duration, convexity

def main(datestr, inputfile, yieldfile):
  def str2date(str):
    return datetime.date(int(str[0:4]),int(str[4:6]),int(str[6:8]))
  cur_date = str2date(datestr)
  pyf = open(yieldfile)
  ycdata = [l for l in pyf]
  
  fin = open(inputfile)
  fout = open("problem-1.txt", "w")
  for l in fin:
    cols = l.strip().split(',')
    res = bondinfo(cols[1], float(cols[2]), str2date(cols[3]), float(cols[4]), float(cols[5]), cur_date, ycdata[1:])
    print >> fout, ",".join([str(x) for x in [cols[0]] + list(res)])
    
if __name__ == "__main__":
  if len(sys.argv) < 7:
    print "Usage: python solution1.py -d YYYYMMDD -i inputfile -y yield file"
    sys.exit()
  inputfile = ""
  yieldfile = ""
  date = ""
  for i in xrange(1, len(sys.argv), 2):
    if sys.argv[i] == "-d":
        date = sys.argv[i+1]
    if sys.argv[i] == "-i":
        inputfile = sys.argv[i+1]
    if sys.argv[i] == "-y":
        yieldfile = sys.argv[i+1]

  if inputfile == "" or yieldfile == "" or date == "":
    print "Usage: python solution1.py -d YYYYMMDD -i inputfile -y yield file"
    sys.exit()
  #print "date:", date, "ifile:", inputfile, "yf:", yieldfile 
  main(date, inputfile, yieldfile)
    
